<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('menu');
            $table->string('titre');
            $table->string('paragraph');
            $table->string('lien');
            $table->string('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_banners');
    }
}
