<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'site.pages.homepage');
Route::get('/category/{slug}', 'Site\CategoryController@show')->name('category.show');
Route::get('/product/{slug}', 'Site\ProductController@show')->name('product.show');

Route::post('/product/add/cart', 'Site\ProductController@addToCart')->name('product.add.cart');
Route::get('/cart', 'Site\CartController@getCart')->name('checkout.cart');
Route::get('/cart/item/{id}/remove', 'Site\CartController@removeItem')->name('checkout.cart.remove');
Route::get('/cart/clear', 'Site\CartController@clearCart')->name('checkout.cart.clear');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/checkout', 'Site\CheckoutController@getCheckout')->name('checkout.index');
    Route::post('/checkout/order', 'Site\CheckoutController@placeOrder')->name('checkout.place.order');

    Route::get('checkout/payment/complete', 'Site\CheckoutController@complete')->name('checkout.payment.complete');

    Route::get('account/orders', 'Site\AccountController@getOrders')->name('account.orders');
});
/* mes propres api */

Route::get('/api/category_list/', 'Site\CategoryController@category_list')->name('api.category_list');
Route::get('/api/category/{id}', 'Site\CategoryController@category')->name('api.category');
Route::get('/api/category_by_slug/{slug}', 'Site\CategoryController@category_by_slug')->name('api.category_by_slug');
Route::get('/api/menubanner/{menu}', 'Moojo\MenuController@menuBanner')->name('api.menuBanner');







Route::get('/api/product_list/', 'Site\ProductController@product_list')->name('api.product_list');
Route::get('/api/product/{id}', 'Site\ProductController@product')->name('api.product');
Route::get('/api/product_by_slug/{slug}', 'Site\ProductController@product_by_slug')->name('api.product_by_slug');




Auth::routes();
require 'admin.php';
