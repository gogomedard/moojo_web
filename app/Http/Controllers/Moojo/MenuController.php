<?php

namespace App\Http\Controllers\Moojo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\menuBanner as M;

class MenuController extends Controller
{
    public function menuBanner($menu)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $mb = M::where('menu',$menu)->get();
        return response()->json($mb);
    }
}


