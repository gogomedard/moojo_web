<?php

namespace App\Http\Controllers\Site;

use Cart;
use Illuminate\Http\Request;
use App\Contracts\ProductContract;
use App\Http\Controllers\Controller;
use App\Contracts\AttributeContract;
use App\Models\Product as P;
use App\Models\ProductImage as I;

class ProductController extends Controller
{
    protected $productRepository;

    protected $attributeRepository;

    public function __construct(ProductContract $productRepository, AttributeContract $attributeRepository)
    {
        $this->productRepository = $productRepository;
        $this->attributeRepository = $attributeRepository;
    }


        public function hello()
    {
      
        return 'hello';
    }


    public function show($slug)
    {
        $product = $this->productRepository->findProductBySlug($slug);
        $attributes = $this->attributeRepository->listAttributes();
        return view('site.pages.product', compact('product', 'attributes'));
    }

    public function addToCart(Request $request)
    {
        $product = $this->productRepository->findProductById($request->input('productId'));
        $options = $request->except('_token', 'productId', 'price', 'qty');

        Cart::add(uniqid(), $product->name, $request->input('price'), $request->input('qty'), $options);

        return redirect()->back()->with('message', 'Item added to cart successfully.');
    }


    public function product_list()
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $product = P::all();
        return response()->json($product);
    }

    public function product($id)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $product = P::find($id);

        return response()->json($product);
    }

    public function product_by_slug($slug)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $i= 0;
        $product = P::where('slug',$slug)->get()->first();
        if($product) $id = $product->id;
        $arr = [];
        array_push($arr,$product);

       // $image = I::where('product_id',$id)->get()->all();
       $image = $product->images()->get()->all();
       $categorie = $product->categories()->get()->all();
      
        array_push($arr,$image);
        array_push($arr,$categorie);

        return response()->json($arr);
    }

}
