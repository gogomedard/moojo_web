<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\CategoryContract;
use App\Models\Category as C;


class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryContract $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function show($slug)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        $category = C::where('slug',$slug)->get()->first();
        //dd($category);

        return view('site.pages.category', compact('category'));
    }

    public function category($id)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $category = C::find($id);

        return response()->json($category);
    }

    public function category_by_slug($slug)
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();

        $category = C::where('slug',$slug)->get();
        return response()->json($category);
    }

    public function category_list()
    {
        //$category = $this->categoryRepository->findBySlug($slug);
        //$product = P::where('slug',$slug)->get()->first();
        $category = C::all()->take(10);
        return response()->json($category);
    }


}
